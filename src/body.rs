use std::fmt;

pub struct Body {
    content: String,
}

impl Body {
    pub fn new() -> Body {
        let content = Default::default();
        Body { content }
    }

    pub fn set_body(&mut self, content: String) {
        self.content = content;
    }
}

impl fmt::Display for Body {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.content)
    }
}

#[cfg(test)]
mod body_tests {
    use super::*;

    #[test]
    fn test_body_display() {
        let mut body = Body::new();
        body.set_body(String::from("<html></html>"));

        assert_eq!("<html></html>", body.to_string());
    }
}
