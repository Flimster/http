//! # Response
//! A collection of Data types that you use to create HTTP responses

use crate::body::Body;
use crate::headers::HeaderMap;
use crate::status_code::StatusCode;

use std::fmt;
const VERSION: &str = "HTTP/1.1";

/// The Response struct that is used to represent HTTP responses
/// Use the Response builder to easily create responses
pub struct Response {
    status_line: StatusLine,
    headers: HeaderMap,
    body: Body,
}

/// A Response builder that uses the builder pattern
/// Makes it easy to create new Responses and add headers and set a body
///
/// # Example
///
/// ```
/// use http::response::ResponseBuilder;
/// let response = ResponseBuilder::new(200)
///     .add_header("Content-Type", "application/json")
///     .create_response();
/// ```
pub struct ResponseBuilder {
    status_line: StatusLine,
    headers: HeaderMap,
    body: Body,
}

impl ResponseBuilder {
    /// Creates a new ResponseBuilder with a given HTTP status code
    ///
    /// # Example
    ///
    /// ```
    /// use http::response::ResponseBuilder;
    /// let response = ResponseBuilder::new(200);
    /// ```
    pub fn new(code: usize) -> ResponseBuilder {
        let status_code: StatusCode = num_traits::FromPrimitive::from_usize(code).unwrap();
        let status_line = StatusLine::new(status_code);
        let headers = HeaderMap::new();
        let body = Body::new();

        ResponseBuilder {
            status_line,
            headers,
            body,
        }
    }

    /// Adds a new header to the HTTP response
    ///
    /// # Example
    ///
    /// ```
    /// use http::response::ResponseBuilder;
    /// let response = ResponseBuilder::new(200)
    ///     .add_header("Content-Type", "application/json")
    ///     .add_header("Server", "")
    ///     .create_response();
    pub fn add_header(mut self, field_name: &str, field_value: &str) -> ResponseBuilder {
        self.headers.add_header(field_name, field_value);
        self
    }

    /// Sets the body of the HTTP response
    ///
    /// # Example
    ///
    /// ```
    /// use http::response::ResponseBuilder;
    /// let response = ResponseBuilder::new(200)
    ///     .add_header("Content-Type", "text/html")
    ///     .add_header("Content-Length", "13")
    ///     .set_body(String::from("<html></html>"))
    ///     .create_response();
    /// ```
    pub fn set_body(mut self, content: String) -> ResponseBuilder {
        self.body.set_body(content);
        self
    }

    /// Consumes the ResponseBuilder and creates the HTTP response
    pub fn create_response(self) -> Response {
        Response {
            status_line: self.status_line,
            headers: self.headers,
            body: self.body,
        }
    }
}

impl fmt::Display for Response {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}\n{}", self.status_line, self.headers, self.body)
    }
}

struct StatusLine {
    status_line: String,
}

impl StatusLine {
    fn new(status: StatusCode) -> StatusLine {
        let status_line = format!("{} {}\r\n", VERSION, status);

        StatusLine { status_line }
    }
}

impl fmt::Display for StatusLine {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.status_line)
    }
}

#[cfg(test)]
mod response_tests {
    use super::*;

    #[test]
    fn test_status_line() {
        let response = ResponseBuilder::new(200).create_response();
        assert_eq!("HTTP/1.1 200 OK\r\n\n", response.to_string());
    }

    #[test]
    fn test_single_header() {
        let response = ResponseBuilder::new(404)
            .add_header("Content-Type", "application/json")
            .create_response();
        let actual = String::from("HTTP/1.1 404 Not Found\r\nContent-Type: application/json\r\n\n");

        assert_eq!(actual, response.to_string());
    }

    #[test]
    fn test_body() {
        let response = ResponseBuilder::new(200)
            .add_header("Content-Type", "text/html")
            .set_body("<html></html>".to_string())
            .create_response();

        let actual = String::from("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\n<html></html>");

        assert_eq!(actual, response.to_string());
    }
}
