#![allow(non_snake_case)]

use std::fmt;

#[derive(Debug, FromPrimitive)]
pub enum StatusCode {
    // 100
    Continue = 100,
    // 101
    SwitchingProtocols = 101,
    //102
    Processing = 102,
    // 103
    EarlyHints = 103,
    // 200
    OK = 200,
    // 201
    Created = 201,
    // 202
    Accepted = 202,
    // 203
    NonAuthoritativeInformation = 203,
    // 204
    NoContent = 204,
    // 205
    ResetContent = 205,
    // 206
    PartialContent = 206,
    // 404
    NotFound = 404,
    // 500
    InternalServerError = 500,
}

impl fmt::Display for StatusCode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (status_code, reason) = self.get_status();
        write!(f, "{} {}", status_code, reason)
    }
}

impl StatusCode {
    fn get_status(&self) -> (usize, &str) {
        match self {
            StatusCode::Continue => (100, "Continue"),

            StatusCode::SwitchingProtocols => (101, "Switching Protocols"),

            StatusCode::Processing => (102, "Processing"),

            StatusCode::EarlyHints => (103, "Early Hints"),

            StatusCode::OK => (200, "OK"),

            StatusCode::Created => (201, "Created"),

            StatusCode::Accepted => (202, "Accepted"),

            StatusCode::NonAuthoritativeInformation => (203, "Non-Authoritative Information"),

            StatusCode::NoContent => (204, "No Content"),

            StatusCode::ResetContent => (205, "Reset Content"),

            StatusCode::PartialContent => (206, "Partial Content"),

            StatusCode::NotFound => (404, "Not Found"),

            StatusCode::InternalServerError => (500, "Internal Server Error"),
        }
    }
}

#[cfg(test)]
mod status_codes_tests {
    use super::*;

    #[test]
    fn test_Continue_status() {
        let status = StatusCode::Continue;
        assert_eq!("100 Continue", status.to_string());
    }

    #[test]
    fn test_SwitchingProtocols_status() {
        let status = StatusCode::SwitchingProtocols;
        assert_eq!("101 Switching Protocols", status.to_string());
    }
    #[test]
    fn test_Processing_status() {
        let status = StatusCode::Processing;
        assert_eq!("102 Processing", status.to_string());
    }

    #[test]
    fn test_EarlyHints_status() {
        let status = StatusCode::EarlyHints;
        assert_eq!("103 Early Hints", status.to_string());
    }

    #[test]
    fn test_OK_status() {
        let status = StatusCode::OK;
        assert_eq!("200 OK", status.to_string());
    }

    #[test]
    fn test_Created_status() {
        let status = StatusCode::Created;
        assert_eq!("201 Created", status.to_string());
    }

    #[test]
    fn test_Accepted_status() {
        let status = StatusCode::Accepted;
        assert_eq!("202 Accepted", status.to_string());
    }

    #[test]
    fn test_NonAuthoritativeInformation_status() {
        let status = StatusCode::NonAuthoritativeInformation;
        assert_eq!("203 Non-Authoritative Information", status.to_string());
    }

    #[test]
    fn test_NoContent_status() {
        let status = StatusCode::NoContent;
        assert_eq!("204 No Content", status.to_string());
    }

    #[test]
    fn test_ResetContent_status() {
        let status = StatusCode::ResetContent;
        assert_eq!("205 Reset Content", status.to_string());
    }

    #[test]
    fn test_PartialContent_status() {
        let status = StatusCode::PartialContent;
        assert_eq!("206 Partial Content", status.to_string());
    }

    #[test]
    fn test_NotFound_status() {
        let status = StatusCode::NotFound;
        assert_eq!("404 Not Found", status.to_string());
    }

    #[test]
    fn test_InternalServerError_status() {
        let status = StatusCode::InternalServerError;
        assert_eq!("500 Internal Server Error", status.to_string());
    }

}
