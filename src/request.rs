//! # Request
//! Contains both the Request and RequestBuilder data types
//! The Request is used to correctly format a HTTP request
//! The RequestBuilder is used to build a request


use crate::body::Body;
use crate::headers::HeaderMap;
use crate::method::Method;
use std::fmt;

/// An HTTP request is composed of a request_line, zero or more headers and finally an optional body
/// This is used to correctly format a HTTP request
pub struct Request {
    start_line: StartLine,
    headers: HeaderMap,
    body: Body,
}

/// The RequestBuilder follows the builder pattern
/// Easily build the request by adding headers and setting the body
///
/// # Example
///
/// ```
/// use http::method::Method;
/// use http::request::RequestBuilder;
///
/// let request = RequestBuilder::new(Method::POST, "/api/cars")
///     .add_header("Content-Type", "application/json")
///     .set_body(String::from("{type: Toyota}"))
///     .create();
/// ```
pub struct RequestBuilder {
    start_line: StartLine,
    headers: HeaderMap,
    body: Body,
}

impl RequestBuilder {
    /// Create a new RequestBuilder with a given start line
    ///
    /// # Example
    ///
    /// ```
    /// use http::method::Method;
    /// use http::request::RequestBuilder;
    ///
    /// let request = RequestBuilder::new(Method::GET, "/")
    ///     .create();
    /// ```
    pub fn new(method: Method, target: &str) -> RequestBuilder {
        let http_version = String::from("HTTP/1.1");
        let target = target.to_owned();
        let start_line = StartLine::new(method, target, http_version);
        let headers = HeaderMap::new();
        let body = Body::new();

        RequestBuilder {
            start_line,
            headers,
            body,
        }
    }

    /// Adds a new header to the HTTP request
    ///
    /// # Example
    ///
    /// ```
    /// use http::method::Method;
    /// use http::request::RequestBuilder;
    ///
    /// let request = RequestBuilder::new(Method::GET, "/")
    ///     .add_header("User-Agent", "Haven-Client")
    ///     .add_header("Connection", "Keep-Alive")
    ///     .create();
    /// ```
    pub fn add_header(mut self, field_name: &str, field_value: &str) -> RequestBuilder {
        self.headers.add_header(field_name, field_value);
        self
    }

    /// Sets the body of the request
    ///
    /// # Example
    ///
    /// ```
    /// use http::method::Method;
    /// use http::request::RequestBuilder;
    ///
    /// let body = String::from("{\"age\":3}");
    /// let request = RequestBuilder::new(Method::POST, "/")
    ///     .add_header("Content-Type", "application/json")
    ///     .add_header("Content-Length", &body.len().to_string())
    ///     .set_body(body)
    ///     .create();
    /// ```
    pub fn set_body(mut self, content: String) -> RequestBuilder {
        self.body.set_body(content);
        self
    }

    /// Consumes the RequestBuilder and returns a Request
    pub fn create(self) -> Request {
        Request {
            start_line: self.start_line,
            headers: self.headers,
            body: self.body,
        }
    }

}

impl Request {
    pub fn method(&self) -> &Method {
        self.start_line.method()
    } 

    pub fn route(&self) -> &String {
        self.start_line.route()
    }
}

impl fmt::Display for Request {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}\n{}", self.start_line, self.headers, self.body)
    }
}

struct StartLine {
    method: Method,
    route: String,
    version: String,
}

impl StartLine {
    fn new(method: Method, route: String, version: String) -> StartLine {
        StartLine {
            method,
            route,
            version,
        }
    }

    fn method(&self) -> &Method {
        &self.method
    }

    fn route(&self) -> &String {
        &self.route
    }
}

impl fmt::Display for StartLine {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?} {} {}\r", self.method, self.route, self.version)
    }
}

#[cfg(test)]
mod request_tests {
    use super::*;

    #[test]
    fn test_start_line() {
        let request = RequestBuilder::new(Method::GET, "/").create();
        let actual = String::from("GET / HTTP/1.1\r\n");
        assert_eq!(actual, request.to_string())
    }

    #[test]
    fn test_method() {
        let request = RequestBuilder::new(Method::GET, "/").create();
        assert_eq!(Method::GET, *request.method());
    }

    #[test]
    fn test_route() {
        let request = RequestBuilder::new(Method::POST, "/api/cars").create();
        let actual = String::from("/api/cars");
        assert_eq!(actual, *request.route());
    }
}
