//! # HTTP
//!
//! `HTTP` is a crate that allows you to create HTTP responses

#[macro_use]
extern crate num_derive;

pub mod method;
pub mod request;
pub mod response;
pub(crate) mod body;
pub(crate) mod headers;
pub(crate) mod status_code;
