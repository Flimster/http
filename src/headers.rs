use std::collections::HashMap;
use std::fmt;
use std::str::FromStr;

pub struct HeaderMap {
    headers: HashMap<String, String>,
}

impl HeaderMap {
    pub fn new() -> HeaderMap {
        let headers = HashMap::new();
        HeaderMap { headers }
    }

    pub fn add_header(&mut self, field_name: &str, field_value: &str) {
        let field_name = String::from_str(field_name).unwrap();
        let field_value = String::from_str(field_value).unwrap();
        self.headers.insert(field_name, field_value);
    }
}

impl fmt::Display for HeaderMap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut thing = String::new();
        for (field_name, field_value) in self.headers.iter() {
            thing.push_str(&format!("{}: {}\r\n", field_name, field_value));
        }
        write!(f, "{}", thing)
    }
}

#[cfg(test)]
mod header_tests {
    use super::*;

    #[test]
    fn test_header_display() {
        let mut headers = HeaderMap::new();
        headers.add_header("Content-Type", "application/json");
        assert_eq!(headers.to_string(), "Content-Type: application/json\r\n");
    }
}
