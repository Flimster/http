#[derive(Debug, PartialEq)]
pub enum Method {
    // Optional body
    // Response has body
    // Safe
    // Idempotent
    // Cacheable
    GET,

    // Optional body
    // No response body
    // Safe
    // Idempotent
    // Cacheable
    HEAD,

    // Requires body
    // Response has body
    // Not safe
    // Not idempotent
    // Cacheable
    POST,

    // Requires body
    // Response has body
    // Not safe
    // Idempotent
    // Not cacheable
    PUT,

    // Optional body
    // Response has body
    // Not safe
    // Idempotent
    // Not cacheable
    DELETE,

    // No body
    // Response has body
    // Safe
    // Idempotent
    // Not cacheable
    TRACE,

    // Optional body
    // Response has body
    // Safe
    // Idempotent
    // Not cacheable
    OPTIONS,

    // Optional body
    // Response has body
    // Not safe
    // Not Idempotent
    // Not cacheable
    CONNECT,

    // Requires body
    // Response has body
    // Not safe
    // Not Idempotent
    // Not cacheable
    PATCH,
}
